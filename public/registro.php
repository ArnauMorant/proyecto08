<?php
require "./../src/reg.php";

  $r=new Equipo();
  $error=$r->comprobarCampos($_POST);

  if(isset($error)){
      if($error===false){
        //NO HAY ERROR
        $r->conectar();
        $r->registrarse();
      }
  }
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Proyecto</title>
    <link rel="stylesheet" href="css/master.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  </head>
  <body>
<?php require "assets/header.php" ?>
    <center>
      <div class="form">
      <form method="post">
      <br>
      <label for="nombre" style="margin-right:380px">Nombre:</label>
      <br>
      <input type="text" name="nombre" id="nombre" style="width:450px">
      <div id="hidden1" style="display:none;">No puede dejarse en blanco</div>
      <br><br>
      <label for="apellidos" style="margin-right:380px">Apellidos:</label>
      <br>
      <input type="text" name="apellidos" id="apellidos" style="width:450px">
      <div id="hidden2" style="display:none;">No puede dejarse en blanco</div>
      <br><br>
      <label for="telefono" style="margin-right:380px">Telefono:</label>
      <br>
      <input type="text" name="telefono" id="telefono" style="width:450px">
      <div id="hidden3" style="display:none;">No puede dejarse en blanco</div>
      <div id="hidden4" style="display:none;">Tiene que utilizar numeros</div>
      <br><br>
      <label for="curso" style="margin-right:400px">Curso:</label>
      <br>
      <input type="text" name="curso" id="curso" style="width:450px">
      <div id="hidden5" style="display:none;">No puede dejarse en blanco</div>
      <div id="hidden6" style="display:none;">Tiene que utilizar numeros</div>
      <br><br>
      <div style="margin-left:380px">
      <input type="submit" value="Enviar" onclick="return comprobar()">
      </div>
      <br>
      <?php echo $registro ?>
    </form>
    </div>
    <script type="text/javascript" src="comprobarRegistro.js"></script>
    </center>
    </body>
  </html>
